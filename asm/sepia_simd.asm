; vim:ft=nasm
extern _GLOBAL_OFFSET_TABLE

global sepia_3pixels_inplace_simd

section .text

%define C_1_1 0.393
%define C_1_2 0.769
%define C_1_3 0.189
%define C_2_1 0.349
%define C_2_2 0.686
%define C_2_3 0.168
%define C_3_1 0.272
%define C_3_2 0.543
%define C_3_3 0.131

%macro LOAD_CONVERT_PIXELS 2
  pxor %1, %1
  pinsrb %1, [%2], 0
  pinsrb %1, [%2], 4
  pinsrb %1, [%2], 8
  pinsrb %1, [%2+3], 12
  cvtdq2ps %1, %1
%endmacro

%macro MUL_AND_ADD_XMM 0
  mulps xmm0, xmm3
  mulps xmm1, xmm4
  mulps xmm2, xmm5

  addps xmm0, xmm1
  addps xmm0, xmm2
%endmacro

%macro SAVE_TRANSFORMED 2
  pextrb byte [%1+0], %2, 0
  pextrb byte [%1+1], %2, 4
  pextrb byte [%1+2], %2, 8
  pextrb byte [%1+3], %2, 12
%endmacro

%macro CONVERT_AND_SAT 1
  cvtps2dq %1, %1
  pminud %1, xmm6
%endmacro

sepia_3pixels_inplace_simd:
  movdqa xmm6, [rel MAX_SATURATION_VALUE]

  LOAD_CONVERT_PIXELS xmm0, rdi
  LOAD_CONVERT_PIXELS xmm1, rdi+1
  LOAD_CONVERT_PIXELS xmm2, rdi+2

  movaps xmm3, [rel FIRST_MATRIX_FIRST_ROW]
  movaps xmm4, [rel FIRST_MATRIX_SECOND_ROW]
  movaps xmm5, [rel FIRST_MATRIX_THIRD_ROW]

  MUL_AND_ADD_XMM
  CONVERT_AND_SAT xmm0
  movaps xmm7, xmm0

  LOAD_CONVERT_PIXELS xmm0, rdi+3
  LOAD_CONVERT_PIXELS xmm1, rdi+4
  LOAD_CONVERT_PIXELS xmm2, rdi+5

  movaps xmm3, [rel SECOND_MATRIX_FIRST_ROW]
  movaps xmm4, [rel SECOND_MATRIX_SECOND_ROW]
  movaps xmm5, [rel SECOND_MATRIX_THIRD_ROW]

  MUL_AND_ADD_XMM
  CONVERT_AND_SAT xmm0
  movaps xmm8, xmm0

  LOAD_CONVERT_PIXELS xmm0, rdi+6
  LOAD_CONVERT_PIXELS xmm1, rdi+7
  LOAD_CONVERT_PIXELS xmm2, rdi+8

  movaps xmm3, [rel THIRD_MATRIX_FIRST_ROW]
  movaps xmm4, [rel THIRD_MATRIX_SECOND_ROW]
  movaps xmm5, [rel THIRD_MATRIX_THIRD_ROW]

  MUL_AND_ADD_XMM
  CONVERT_AND_SAT xmm0

  SAVE_TRANSFORMED rdi, xmm7
  SAVE_TRANSFORMED rdi+4, xmm8
  SAVE_TRANSFORMED rdi+8, xmm0

  ret 

section .rodata
align 16
MAX_SATURATION_VALUE: dd 255, 255, 255, 255

align 16
;                              B,     G,     R,     B
FIRST_MATRIX_FIRST_ROW:     dd C_3_3, C_2_3, C_1_3, C_3_3
align 16
FIRST_MATRIX_SECOND_ROW:    dd C_3_2, C_2_2, C_1_2, C_3_2
align 16
FIRST_MATRIX_THIRD_ROW:     dd C_3_1, C_2_1, C_1_1, C_3_1

;                              G,     R,     B,     G
align 16
SECOND_MATRIX_FIRST_ROW:    dd C_2_3, C_1_3, C_3_3, C_2_3
align 16
SECOND_MATRIX_SECOND_ROW:   dd C_2_2, C_1_2, C_3_2, C_2_2
align 16
SECOND_MATRIX_THIRD_ROW:    dd C_2_1, C_1_1, C_3_1, C_2_1

align 16
;                              R,     B,     G,     R
THIRD_MATRIX_FIRST_ROW:     dd C_1_3, C_3_3, C_2_3, C_1_3
align 16
THIRD_MATRIX_SECOND_ROW:    dd C_1_2, C_3_2, C_2_2, C_1_2
align 16
THIRD_MATRIX_THIRD_ROW:     dd C_1_1, C_3_1, C_2_1, C_1_1
