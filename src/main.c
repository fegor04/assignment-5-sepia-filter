#include "bmp.h"
#include "deserializer.h"
#include "image.h"
#include "sepia.h"
#include "sepia_simd.h"
#include "serializer.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/time.h>

const size_t expected_number_of_arguments = 2; // input file, output file

enum ERROR_CODE {
  BAD_USAGE,
  BAD_INPUT_FILE,
  BAD_OUTPUT_FILE,
  TRANSFORM_ERROR,
};

int main(int argc, char **argv) {

#ifndef DEBUG
  if (argc - 1 != (int)expected_number_of_arguments) {
    fprintf(stderr, "Usage: ./sepia <input-file> <output-file>");
    exit(BAD_USAGE);
  }
  char *input_file_name = argv[1];
  char *output_file_name = argv[2];
#else
  char *input_file_name = "samples/kitty.bmp";
  char *output_file_name = "output.bmp";
#endif

  FILE *input = fopen(input_file_name, "rb");
  if (input == NULL) {
    perror("Could not open input file");
    exit(BAD_INPUT_FILE);
  }

  FILE *output = fopen(output_file_name, "wb");
  if (output == NULL) {
    perror("Could not open output file");
    exit(BAD_OUTPUT_FILE);
  }

  struct image img;
  enum read_status read_status = read_from_bmp(input, &img);
  if (read_status != 0) {
    fprintf(stderr, "Could not read bmp file. ERROR_CODE: %d", read_status);
    exit(BAD_INPUT_FILE);
  }

  struct rusage r;
  getrusage(RUSAGE_SELF, &r);
  struct timeval start = r.ru_utime;

#ifdef SIMD
  sepia_simd_inplace(&img);
#else
  sepia_inplace(&img);
#endif

  getrusage(RUSAGE_SELF, &r);
  struct timeval end = r.ru_utime;
  uint64_t res =
      ((end.tv_sec - start.tv_sec) * 1000000L) + (end.tv_usec - start.tv_usec);

  enum write_status write_status = save_as_bmp(output, &img);
  if (write_status != 0) {
    fprintf(stderr, "Could not read bmp file. ERROR_CODE: %d", read_status);
    exit(BAD_OUTPUT_FILE);
  }

  printf("Time to perform transform: %" PRIu64 " microseconds\n", res);

  free(img.data);
}
