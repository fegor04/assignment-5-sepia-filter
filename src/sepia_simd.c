#include "sepia_simd.h"
#include "pixel.h"

extern void sepia_3pixels_inplace_simd(struct pixel *p);
extern void sepia_pixel_inplace(struct pixel *p);

void sepia_simd_inplace(struct image *img) {
  size_t pixels_count = img->width * img->height;
  size_t simd_pixels_count = pixels_count - pixels_count % 4;
  for (size_t i = 0; i < simd_pixels_count; i += 4) {
    struct pixel *cur = img->data + i;
    sepia_3pixels_inplace_simd(cur);
  }
  for (size_t i = simd_pixels_count; i < pixels_count; i++) {
    sepia_pixel_inplace(img->data + i);
  }
}
