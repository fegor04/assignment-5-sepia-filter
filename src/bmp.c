#include "bmp.h"

#include <stdint.h>
#include <stdio.h>

#include "deserializer.h"
#include "image.h"
#include "pixel.h"
#include "serializer.h"

const char BYTES_PER_PIXEL = 3; // r, g, b;
const uint16_t BMP_MAGIC_BYTES = 0x4D42;
const uint32_t BMP_VER3_HEADER_SIZE = 40;

enum BMP_COMPRESSION { BI_RGB = 0 };

struct bmp_header {
  /**
   * Отметка для отличия формата от других
   * Одно из возможных значений - BMP_MAGIC_BYTES
   */
  uint16_t bfType;
  /**
   * Размер файла в байтах
   */
  uint32_t bfileSize;
  /**
   * Должен содержать 0
   */
  uint32_t bfReserved;
  /**
   * Положение пиксельных данных относительно начала данной структуры.
   */
  uint32_t bOffBits;
  /**
   * Размер заголовка в байтах
   */
  uint32_t biSize;
  /**
   * Ширина изображения в пикселях
   */
  uint32_t biWidth;
  /**
   * Высота изображения в пикселях
   */
  uint32_t biHeight;
  uint16_t biPlanes;
  /**
   * Количество битов на пиксель
   */
  uint16_t biBitCount;
  /**
   * Используемый метод компрессии
   */
  uint32_t biCompression;
  /**
   * Размер изображения (bitmap)
   */
  uint32_t biSizeImage;
  /**
   * Горизонтальное разрешение изображения, пиксель / метр
   */
  uint32_t biXPelsPerMeter;
  /**
   * Вертикальное разрешение изображения, пиксель / метр
   */
  uint32_t biYPelsPerMeter;
  /**
   * Количество цветов в цветовой палитре.
   * 0 = 2^n
   */
  uint32_t biClrUsed;
  /**
   * Количество важных цветов в цветовой палитре.
   * 0 = все цвета важные
   */
  uint32_t biClrImportant;
} __attribute__((packed));

static uint16_t compute_padding(uint64_t row_length_bytes) {
  return (4 - (row_length_bytes) % 4) % 4;
}

static struct bmp_header bmp_header_for_image(const struct image *img) {
  uint64_t row_width_bytes = img->width * BYTES_PER_PIXEL;
  uint16_t padding = compute_padding(row_width_bytes);
  uint16_t bmp_row_width = row_width_bytes + padding;

  struct bmp_header header;
  header.bfType = BMP_MAGIC_BYTES;
  header.bfileSize = sizeof(header) + bmp_row_width * img->height;
  header.bfReserved = 0;
  header.bOffBits = sizeof(header);
  header.biSize = BMP_VER3_HEADER_SIZE;
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biPlanes = 1;
  header.biBitCount = BYTES_PER_PIXEL * 8;
  header.biCompression = BI_RGB;
  header.biSizeImage = bmp_row_width * img->height;
  header.biXPelsPerMeter = 1;
  header.biYPelsPerMeter = 1;
  header.biClrUsed = 0;
  header.biClrImportant = 0;
  return header;
}

enum read_status read_from_bmp(FILE *in, struct image *img) {
  struct bmp_header header;
  const unsigned long header_bytes_read = fread(&header, sizeof(header), 1, in);
  if (header_bytes_read == 0) {
    return READ_INVALID_SIZE;
  }
  img->height = header.biHeight;
  img->width = header.biWidth;
  img->data = malloc(sizeof(struct pixel) * header.biHeight * header.biWidth);
  if (img->data == NULL) {
    return READ_FAILED_TO_MALLOC;
  }

  const uint64_t row_padding = header.biWidth * BYTES_PER_PIXEL;
  const uint16_t padding = compute_padding(row_padding);
  if (header.bOffBits < sizeof(header)) {
    free(img->data);
    return READ_INVALID_HEADER;
  }
  fseek(in, (long)(header.bOffBits - sizeof(header)), SEEK_CUR);

  for (uint32_t row = 0; row < header.biHeight; row++) {
    const unsigned long read_items =
        fread(img->data + row * header.biWidth, row_padding, 1, in);
    if (read_items == 0) {
      return READ_INVALID_SIZE;
    }

    fseek(in, padding, SEEK_CUR);
  }
  return READ_OK;
}

enum write_status save_as_bmp(FILE *out, const struct image *img) {
  const uint64_t row_length = img->width * BYTES_PER_PIXEL;
  const uint16_t padding = (4 - (row_length) % 4) % 4;

  const struct bmp_header header = bmp_header_for_image(img);
  const unsigned long header_items_written =
      fwrite(&header, sizeof(header), 1, out);
  if (header_items_written == 0) {
    return WRITE_UNKNOWN_ERROR;
  }

  for (uint32_t row = 0; row < img->height; row++) {
    const unsigned long body_items_written =
        fwrite(img->data + row * img->width, row_length, 1, out);
    if (body_items_written == 0) {
      return WRITE_UNKNOWN_ERROR;
    }
    fseek(out, padding, SEEK_CUR);
  }

  return WRITE_OK;
}
