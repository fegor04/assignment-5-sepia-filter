#include "sepia.h"
#include "stdio.h"

static unsigned char sat(uint64_t x) { return x < 256 ? x : 255; }

static const float c[3][3] = {
    {.393f, .796f, .189f}, {.349f, .686f, .168f}, {.272f, .543f, .131f}};

void sepia_pixel_inplace(struct pixel *pixel) {
  const struct pixel old = *pixel;
  pixel->r = sat(old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2]);
  pixel->g = sat(old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2]);
  pixel->b = sat(old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2]);
}

struct maybe_image sepia(const struct image img) { return some_image(img); }

void sepia_inplace(struct image *img) {
  for (size_t i = 0; i < img->height * img->width; i++) {
    struct pixel *cur = img->data + i;
    sepia_pixel_inplace(cur);
  }
}
