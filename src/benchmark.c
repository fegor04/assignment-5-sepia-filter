#include "bmp.h"
#include "deserializer.h"
#include "image.h"
#include "pixel.h"
#include "sepia.h"
#include "sepia_simd.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>

struct image read_from_bmp_or_exit(FILE *file) {
  struct image img;
  const enum read_status status = read_from_bmp(file, &img);
  if (status != READ_OK) {
    fprintf(stderr, "Could not read bmp file. Error: %d\n", status);
    exit(-1);
  }
  return img;
}

struct image image_clone_or_panic(const struct image *img) {
  struct pixel *data = malloc(img->width * img->height * sizeof(struct pixel));
  if(data == NULL) {
    perror("Could not allocate memory for image clone.");
    exit(-1);
  }
  return (struct image){img->width, img->height, data};
}

float compute_average(const uint64_t arr[], size_t arr_size) {
  float sum = 0;
  for (size_t i = 0; i < arr_size; i++) {
    sum += arr[i];
  }
  return sum / arr_size;
}

float compute_corrected_variance(const uint64_t arr[], size_t arr_size) {
  float res = 0;
  float mean = compute_average(arr, arr_size);
  for (size_t i = 0; i < arr_size; i++) {
    float a = (arr[i] - mean);
    res += a * a;
  }
  return res / (arr_size - 1);
}

int main(int argc, char **argv) {
  // gamma = 0.95; Phi(t_gamma) = gamma / 2;
  const float gamma = 0.95;
  const float t_gamma = 1.96;

  (void)argc;
  (void)argv;
  size_t runs_count = 1000;
  const char *input_file_name = "samples/kitty.bmp";
  FILE *input_file = fopen(input_file_name, "r");
  const struct image img = read_from_bmp_or_exit(input_file);
  struct image img1 = image_clone_or_panic(&img);

  uint64_t simd_times[runs_count];
  uint64_t c_times[runs_count];

  size_t image_size = img.height * img.width * sizeof(struct pixel) / 1024;
  printf("Image size: %zu KiB\n", image_size);

  struct rusage r;

  for (size_t i = 0; i < runs_count; i++) {
    memcpy(img1.data, img.data, img.width * img.height * sizeof(struct pixel));
    getrusage(RUSAGE_SELF, &r);
    const struct timeval start = r.ru_utime;
    sepia_simd_inplace(&img1);
    getrusage(RUSAGE_SELF, &r);
    const struct timeval end = r.ru_utime;
    simd_times[i] =
        ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
  }

  float simd_corrected_variance =
      compute_corrected_variance(simd_times, runs_count);
  float simd_average = compute_average(simd_times, runs_count);
  float simd_delta =
      t_gamma * sqrtf(simd_corrected_variance) / sqrt(runs_count);
  printf("Confidence interval (%.2f) for SIMD benchmark:\t\t%.2f +- %.2f "
         "(%.2f%%) microseconds\n",
         gamma, simd_average, simd_delta, simd_delta / simd_average * 100);

  for (size_t i = 0; i < runs_count; i++) {
    memcpy(img1.data, img.data, img.width * img.height * sizeof(struct pixel));
    getrusage(RUSAGE_SELF, &r);
    const struct timeval start = r.ru_utime;
    sepia_inplace(&img1);
    getrusage(RUSAGE_SELF, &r);
    const struct timeval end = r.ru_utime;
    c_times[i] =
        ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
  }

  float c_corrected_variance = compute_corrected_variance(c_times, runs_count);
  float c_average = compute_average(c_times, runs_count);
  float c_delta = t_gamma * sqrtf(c_corrected_variance) / sqrt(runs_count);
  printf("Confidence interval (%.2f) for C benchmark:\t\t%.2f +- %.2f (%.2f%%) "
         "microseconds\n",
         gamma, c_average, c_delta, c_delta / c_average * 100);

  free(img1.data);
  free(img.data);
}
