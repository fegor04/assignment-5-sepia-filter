#include "maybe.h"

struct maybe_image some_image(const struct image value) {
  return (struct maybe_image){.value = value, .is_ok = true};
}

const struct maybe_image none_image = {.is_ok = false};
