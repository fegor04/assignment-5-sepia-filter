#include <stdlib.h>

#include "transformations.h"

/**
 * Поворачивает картинку `source` на 90 градусов по часовой стрелке.
 * Вовзращает `none_image` в случае ошибки.
 */
transformation image_transform_rotate;
