#pragma once
#include <stdint.h>

#include "pixel.h"

struct image {
  uint64_t width, height;
  struct pixel *data;
};
