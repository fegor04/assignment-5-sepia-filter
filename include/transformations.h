#include "image.h"
#include "maybe.h"

typedef struct maybe_image transformation(const struct image source);
typedef void transformation_inplace(struct image *source);
