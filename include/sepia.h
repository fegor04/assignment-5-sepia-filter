#ifndef _SEPIA_H
#define _SEPIA_H
#include "transformations.h"

transformation sepia;
transformation_inplace sepia_inplace;
#endif
