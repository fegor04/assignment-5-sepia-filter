#pragma once
#include <stdio.h>

#include "image.h"

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_SIZE,
  READ_FAILED_TO_MALLOC,
};

typedef enum read_status deserializer(FILE *, struct image *);
