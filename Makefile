SANITIZER := address

CC 				:= clang
CFLAGS 		:= -O3 -Wall -Wextra -fsanitize=$(SANITIZER) -pedantic -c -g -fPIC
DEPFLAGS 	:= -MMD -MP
CPPFLAGS	:= -Iinclude
LDFLAGS 	:= -fuse-ld=lld -lm -fsanitize=$(SANITIZER)

ASMC	 		:= nasm
ASMFLAGS 	:= -felf64 -g

SRC_DIR 	:= src
OBJ_DIR 	:= obj
BIN_DIR 	:= bin
ASM_DIR 	:= asm
SAMPLES_DIR := samples

SRC				:= $(wildcard $(SRC_DIR)/*.c)
ASM				:= $(wildcard $(ASM_DIR)/*.asm)
OBJ_C			:= $(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.c.o)
OBJ_ASM		:= $(ASM:$(ASM_DIR)/%.asm=$(OBJ_DIR)/%.asm.o)

BIN_C 		:= $(BIN_DIR)/main_c
BIN_ASM 	:= $(BIN_DIR)/main_asm
BIN_BENCH := $(BIN_DIR)/benchmark

SAMPLES := $(SAMPLES_DIR)/kitty $(SAMPLES_DIR)/lab3 $(SAMPLES_DIR)/huge
SAMPLES_C := $(SAMPLES:=_output_c.bmp)
SAMPLES_ASM := $(SAMPLES:=_output_asm.bmp)

FORMAT_STYLE := LLVM

.PHONY: clean format

all: $(BIN_C) $(BIN_ASM) $(BIN_BENCH) $(SAMPLES_C) $(SAMPLES_ASM)

$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(DEPFLAGS) $(CFLAGS) $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/%.asm.o: $(ASM_DIR)/%.asm | $(OBJ_DIR)
	$(ASMC) -MP -MD $@.d $(ASMFLAGS) $< -o $@

$(OBJ_DIR)/main_c.c.o: $(SRC_DIR)/main.c | $(OBJ_DIR)
	$(CC) $(DEPFLAGS) $(CFLAGS) $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/main_asm.c.o: $(SRC_DIR)/main.c | $(OBJ_DIR)
	$(CC) -DSIMD $(DEPFLAGS) $(CFLAGS) $(CPPFLAGS) -o $@ $<

$(BIN_C): $(filter-out $(OBJ_DIR)/main.c.o $(OBJ_DIR)/benchmark.c.o, $(OBJ_C)) $(OBJ_ASM) $(OBJ_DIR)/main_c.c.o | $(BIN_DIR)
	$(CC) $(LDFLAGS) -o $@ $^

$(BIN_ASM): $(filter-out $(OBJ_DIR)/main.c.o $(OBJ_DIR)/benchmark.c.o, $(OBJ_C)) $(OBJ_ASM) $(OBJ_DIR)/main_asm.c.o | $(BIN_DIR)
	$(CC) -DSIMD $(DEPFLAGS) $(LDFLAGS) -o $@ $^

$(BIN_BENCH): $(filter-out $(OBJ_DIR)/main.c.o, $(OBJ_C)) $(OBJ_ASM) | $(BIN_DIR)
	$(CC) $(DEPFLAGS) $(LDFLAGS) -o $@ $^

$(BIN_DIR) $(OBJ_DIR):
	mkdir -p $@

clean:
	rm -f samples/*output*.bmp
	rm -rf $(OBJ_DIR) $(BIN_DIR)

samples/%_output_asm.bmp: samples/%.bmp $(BIN_ASM)
	$(BIN_ASM) $< $@

samples/%_output_c.bmp: samples/%.bmp $(BIN_C)
	$(BIN_C) $< $@

format:
	clang-format -style=$(FORMAT_STYLE) -i src/*.c include/*.h

tidy:
	clang-tidy src/*.c include/*.h

$(SAMPLES_DIR)/huge.bmp:
	wget https://filesamples.com/samples/image/bmp/sample_5184%C3%973456.bmp -O 

-include $(OBJ_ASM:.o=.d)
-include $(OBJ_C:.o=.d)
